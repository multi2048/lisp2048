(defclass lisp2048 ()
  ((board :accessor board)
   (score :accessor score)
   (turns :accessor turns))
  (setq board '((0 0 0 0) (0 0 0 0) (0 0 0 0) (0 0 0 0))
  (setq score 0)
  (setq turns 0)
  (:documentation "The base class of a game of 2048."))

(defmethod addnum ((object lisp2048))
  (:documentation "Adds a number to the board."))

(defmethod invert ((object lisp2048))
  (:documentation "Switches the X and Y axis on the board."))

(defmethod mirror ((object lisp2048))
  (:documentation "Mirrors the board left-right."))

(defmethod pushlt ((object lisp2048))
  (:documentation "Pushes the numbers on the board to the left."))

(defmethod pushrt ((object lisp2048))
  (:documentation "Pushes the numbers on the board to the right."))

(defmethod pushup ((object lisp2048))
  (:documentation "Pushes the numbers on the board up."))

(defmethod pushdn ((object lisp2048))
  (:documentation "Pushes the numbers on the board down."))

